package sorting.selection;

import java.util.ArrayList;
import java.util.Arrays;

public class SelectionSorting {
    public static void main(String[] args) {
        int[] array = {7, 8, 4, 5, 1, 6, 9, 3, 2};
        ArrayList arrayList = new ArrayList<>(Arrays.asList(7, 8, 4, 5, 1, 6, 9, 3, 2, 10));

        System.out.println("Исходный массив - " + Arrays.toString(array));
        System.out.println("Исходный ArrayList - " + arrayList);
        arraySorting(array);
        arrayListSorting(arrayList);
        System.out.println("\nОтсортированный массив - " + Arrays.toString(array));
        System.out.println("Отсортированный ArrayList - " + arrayList);
    }

    public static int[] arraySorting(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int minIndex = i;
            for (int j = i+1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    minIndex = j;
                }
            }
            if (i != minIndex) {
                int temp = array[i];
                array[i] = array[minIndex];
                array[minIndex] = temp;
            }
        }
        return array;
    }

    public static ArrayList<Integer> arrayListSorting(ArrayList<Integer> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            int min = arrayList.get(i);
            int minIndex = i;
            for (int j = i+1; j < arrayList.size(); j++) {
                if (arrayList.get(j) < min) {
                    min = arrayList.get(j);
                    minIndex = j;
                }
            }
            if (i != minIndex) {
                int temp = arrayList.get(i);
                arrayList.set(arrayList.get(i), arrayList.get(minIndex));
                arrayList.set(arrayList.get(minIndex), temp);
            }
        }
        return arrayList;
    }
}

