package sorting.fibonachi_numbers;
import java.util.Scanner;

public class FibonachiNumbers {
    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        System.out.println("Введите число - ");
        int number= read.nextInt();
        if(number<0){
            System.out.println("Число не может быть меньше нуля");
            return;
        }
        System.out.print("1 1 ");
        calc(number);
    }

    private static void calc(int number) {
        long f1=1;
        long f2=1;
        long f3;
        for (int i = 1; i <number-1; i++) {
            f3=f1+f2;
            System.out.print(f3+" ");
            f1=f2;
            f2=f3;
        }
    }
}