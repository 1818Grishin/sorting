package sorting.binarySearch;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class BinarySearch {
    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);

        int[] array = {-7, -5, 0, 5, 9, 12, 18, 23, 35, 48, 56, 73, 89, 97, 104};
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(-7, -5, 0, 5, 9, 12, 18, 23, 35, 48, 56, 73, 89, 97, 104));

        System.out.print("Введите число - ");
        int number = read.nextInt();
        Arrays.sort(array);
        Collections.sort(arrayList);

        int methodResultArrayList = binarySearch(arrayList, number);
        int methodResult = binarySearch(array, number);
        if (methodResult == -1) {
            System.out.println("Такого числа нет в массиве.");
        } else {
            System.out.println("Введенное число находится в " + methodResult + " ячейке массива");
        }

        if (methodResultArrayList == -1) {
            System.out.println("Такого числа нет в ArrayList'e.");
        } else {
            System.out.println("Введенное число находится в " + methodResultArrayList + " ячейке ArrayList'a");
        }
    }

    private static int binarySearch(int[] array, int number) {
        int first = 0;
        int last = array.length - 1;
        while (first <= last) {
            int mid = (last + first) / 2;
            if (array[mid] < number) {
                first = mid + 1;
            } else if (array[mid] > number) {
                last = mid - 1;
            } else if (array[mid] == number) {
                return mid;
            }
        }
        return -1;
    }

    private static int binarySearch(ArrayList<Integer> arrayList, int number) {
        int first = 0;
        int last = arrayList.size() - 1;
        while (first <= last) {
            int mid = (last + first) / 2;
            if (arrayList.get(mid) == number) {
                return mid;
            } else if (arrayList.get(mid) < number) {
                first = mid + 1;
            } else if (arrayList.get(mid) > number) {
                last = mid - 1;
            }
        }
        return -1;
    }

}