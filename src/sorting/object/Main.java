package sorting.object;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("Бузова", 2002);
        Person person2 = new Person("Басков", 1970);
        Person person3 = new Person("Смирнов", 2003);
        Person person4 = new Person("Иванов", 1992);
        Person person5 = new Person("Иванова", 1992);
        Person person6 = new Person("Николаев", 1982);
        Person person7 = new Person("Бузова", 1973);

        ArrayList<Person> arrayList = new ArrayList<>(Arrays.asList(person1,person2,person3,person4,person5,person6,person7));
        ArrayList<Person> sameSurname = new ArrayList<>();
        print(surnameSorting(arrayList));
        print(birthYearSorting(arrayList));
        printSameSurnames(sameSurname(arrayList, sameSurname));
    }

    private static ArrayList surnameSorting(ArrayList<Person> arrayList) {
        Person temp;

        for (int i = 0; i < arrayList.size(); i++) {
            for (int j = 1+i; j < arrayList.size(); j++) {
                int compare = arrayList.get(j).getSurname().compareTo(arrayList.get(i).getSurname());
                if (compare < 0) {
                    temp = arrayList.get(i);
                    arrayList.set(i, arrayList.get(j));
                    arrayList.set(j, temp);
                }

                if (compare == 1) {
                    if (arrayList.get(j).getBirthYear() < arrayList.get(i).getBirthYear()) {
                        temp = arrayList.get(i);
                        arrayList.set(i, arrayList.get(j));
                        arrayList.set(j, temp);
                    }
                }
            }
        }
        return arrayList;
    }

    private static ArrayList birthYearSorting (ArrayList<Person> arrayList) {
        Person temp;

        for (int i = 0; i < arrayList.size(); i++) {
            for (int j = i + 1; j < arrayList.size(); j++) {
                if (arrayList.get(j).getBirthYear() > arrayList.get(i).getBirthYear()) {
                    temp = arrayList.get(i);
                    arrayList.set(i, arrayList.get(j));
                    arrayList.set(j, temp);
                }
            }
        }
        return arrayList;
    }

    private static ArrayList sameSurname (ArrayList<Person> arrayList, ArrayList<Person> sameSurname) {
        Scanner read = new Scanner(System.in);
        System.out.print("Введите фамилию: ");
        String surname = read.nextLine();
        for (int i = 0; i < arrayList.size(); i++) {
            if (surname.equals(arrayList.get(i).getSurname())) {
                sameSurname.add(arrayList.get(i));
            } else {
                break;
            }
        }
        return sameSurname;
    }

    private static void print (ArrayList<Person> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }
        System.out.println();
    }

    private static void printSameSurnames (ArrayList<Person> sameSurname) {
        if (sameSurname.isEmpty()) {
            System.out.println("Такой фамилии нет в списке.");
        } else {
            for (int i = 0; i < sameSurname.size(); i++) {
                System.out.println(sameSurname.get(i));
            }
        }
    }
}
