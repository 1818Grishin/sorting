package sorting.object;

public class Person {
    private String surname;
    private int birthYear;

    public Person(String surname, int birthYear) {
        this.surname = surname;
        this.birthYear = birthYear;
    }

    public String getSurname() {
        return surname;
    }

    public int getBirthYear() {
        return birthYear;
    }

    @Override
    public String toString() {
        return "Person{" +
                "surname='" + surname + '\'' +
                ", birthYear=" + birthYear +
                '}';
    }
}
