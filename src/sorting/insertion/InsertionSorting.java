package sorting.insertion;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Класс для сортировки массива/ArrayList'a методом вставки
 *
 * @author Zakhar
 */
public class InsertionSorting {
    public static void main (String[] args) {
        int[] array = {7, 5, 9, 8, 4, 1, 3, 2, 6};
        ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(7, 5, 9, 8, 4, 1, 3, 2, 6));
        System.out.println("ArrayList - " + arrayList);
        System.out.println("Array - " + Arrays.toString(array) + "\n");

        insertSort(arrayList);
        insertSort(array);

        System.out.println("Sorted ArrayList - " + arrayList);
        System.out.println("Sorted Array - " + Arrays.toString(array));
    }

    /**
     * Сортирует массив (@code array) по возрастанию значений методом вставки
     *
     * @param array массив целых чисел
     */
        private static void insertSort(int[] array) {
            for (int out = 0; out < array.length; out++) {
                int temp = array[out];
                int in = out;
                while (in > 0 && array[in-1] >= temp) {
                    array[in] = array[in-1];
                    in--;
                }
                array[in] = temp;
            }
        }

    /**
     * Сортирует ArrayList (@code arrayList) по убыванию значений методом вставки
     *
     * @param arrayList массив(?) целых чисел
     */
        private static void insertSort(ArrayList<Integer> arrayList) {
            for (int out = 0; out < arrayList.size(); out++) {
                int temp = arrayList.get(out);
                int in = out;
                while (in > 0 && arrayList.get(in-1) <= temp) {
                    arrayList.set(in, arrayList.get(in-1));
                    in--;
                }
                arrayList.set(in, temp);
            }
        }

}
