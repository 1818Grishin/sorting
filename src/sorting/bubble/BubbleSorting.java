package sorting.bubble;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Класс для сортировки массива/ArrayList'a пузырьковым методом
 *
 * @author Zakhar
 */
public class BubbleSorting {
    public static void main(String[] args) {
        int array[] = {7, 3, 6, 2, 4, 5, 1, 9, 8};
        ArrayList<Integer> arrayList = new ArrayList<> (Arrays.asList(7, 3, 6, 2, 4, 5, 1, 9, 8));
        System.out.println("Исходный массив - " + Arrays.toString(array));
        System.out.println("Исходный ArrayList - " + arrayList + "\n");

        arraySorting(array);
        arrayListSorting(arrayList);

        System.out.println("Отсортированный массив - " + Arrays.toString(array));
        System.out.println("Отсортированный ArrayList - " + arrayList);
    }

    /**
     * Сортирует массив (@code array) по возрастанию значений пузырьковым методом
     *
     * @param array массив целых чисел
     * @return отсортированный массив
     */
    private static int[] arraySorting(int array[]) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length-1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;
    }

    /**
     * Сортирует массив(?) (@code arrayList) по возрастанию значений пузырьковым методом
     *
     * @param arrayList массив(?) целых чисел
     * @return отсортированный массив(?)
     */
    private static ArrayList<Integer> arrayListSorting(ArrayList<Integer> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            for (int j = 0; j < arrayList.size()-1; j++) {
                if (arrayList.get(j) < arrayList.get(j+1)) {
                    int temp = arrayList.get(j);
                    arrayList.set(j, arrayList.get(j+1));
                    arrayList.set(j+1, temp);
                }
            }
        }
        return arrayList;
    }
}