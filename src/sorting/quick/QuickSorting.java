package sorting.quick;

import java.util.Arrays;

public class QuickSorting {
    public static void main(String[] args) {
        int[] array = {7, 3, 6, 2, 4, 5, 1, 9, 8};
        int min = 0;
        int max = array.length - 1;

        System.out.println("Исходный массив: " + Arrays.toString(array));
        arraySorting(array, min, max);
        System.out.println("Отсортированный массив: " + Arrays.toString(array));
    }

    private static void arraySorting(int[] array, int min, int max) {
        int middle = min + (max - min) / 2;
        int average = array[middle];
        int i = min;
        int j = max;
        while (i <= j) {
            while (array[i] < average) {
                i++;
            }
            while (array[j] > average) {
                j--;
            }

            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }

        if (min < j) {
            arraySorting(array, min, j);
        }
        if (max > i) {
            arraySorting(array, i, max);
        }
    }
}
